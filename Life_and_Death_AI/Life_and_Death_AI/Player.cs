﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Life_and_Death_AI
{

    abstract class Player
    {
        static int player_count;
        int player_id;
        
        public Player()
        {
            player_id = player_count;
            player_count++; 
        }
        ~Player()
        {
            player_count--; 
        }

        static public bool operator == (Player p1, Player p2)
        {
            if((object)p1 == null || (object)p2 == null)
            {
                return false; 
            }
            
            if( p1.player_id == p2.player_id)
            {
                return true; 
            }
            return false; 
        }
        static public bool operator !=(Player p1, Player p2)
        {
            if (p1.player_id == p2.player_id)
            {
                return false;
            }
            return true;
        }

        abstract public GameAction DoPlay(GameEngine engine);
    }
}

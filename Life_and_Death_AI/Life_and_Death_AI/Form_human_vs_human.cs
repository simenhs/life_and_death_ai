﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Life_and_Death_AI
{
    public partial class Form_human_vs_human : Form
    {
        const int menu_size = 80;
        const int border_outer = 10;
        const int border_inner = 3;

        GameEngine engine;
        HumanPlayer p1;
        HumanPlayer p2;
        bool isKilling = false;
        bool isBrithing = false;

        Rectangle [,] mapRects;

        public Form_human_vs_human()
        {
            InitializeComponent();
        }

        

        private void Form_human_vs_human_SizeChanged(object sender, EventArgs e)
        {
            this.buttonStartGame.Location = new System.Drawing.Point(this.Size.Width / 2 - this.buttonStartGame.Width / 2,
                this.Size.Height / 2- this.buttonStartGame.Height / 2);
                   
        }

        private void buttonStartGame_Click(object sender, EventArgs e)
        {
            this.buttonStartGame.Hide();
            p1 = new HumanPlayer();
            p2 = new HumanPlayer();
            engine = new GameEngine(p1, p2, 16,18, 50,100);

            this.initalize_rectangles(engine);
            this.DrawBoard(engine);

            this.radioButtonKill.Show();
            this.radioButtonBirth.Show();
            this.radioButtonPass.Show();
            this.radioButtonPass.Checked = true; 
            this.button2.Show();
        }

        private void initalize_rectangles(GameEngine engine)
        {
            mapRects = new Rectangle [engine.boardWidth,engine.boardHeight] ; 

            int cell_size_x = (int)Math.Round((pictureBoxMap.Width ) / (float)engine.boardWidth );
            int cell_size_y = (int)Math.Round((pictureBoxMap.Height ) / (float)engine.boardHeight );

            for (int x = 0; x < engine.boardWidth; x++)
            {
                for (int y = 0; y < engine.boardHeight; y++)
                {
                    mapRects[x, y] = new Rectangle( x * (cell_size_x ),y * (cell_size_y )
                                    , cell_size_x- border_inner, cell_size_y- border_inner);
                }
                
            }
        }

        private void DrawBoard(GameEngine engine)
        {
            
            pictureBoxMap.Visible = true;
            pictureBoxMap.Refresh();
          
            System.Drawing.SolidBrush myBrush;
            System.Drawing.Graphics graphics = pictureBoxMap.CreateGraphics();
            graphics.Clear(this.BackColor);

            for (int x = 0; x < engine.boardWidth; x++)
            {
                for (int y = 0; y < engine.boardHeight; y++)
                {
                    if (engine.Board[x, y].IsDead)
                    {
                        myBrush = new System.Drawing.SolidBrush(System.Drawing.Color.White);
                    }
                    else if (engine.Board[x, y].Owner == p1)
                    {
                        myBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Red);
                    }
                    else if (engine.Board[x, y].Owner == p2)
                    {
                        myBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Blue);
                    }
                    else
                    {
                        myBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
                    }
                    graphics.FillRectangle(myBrush, mapRects[x,y]);
                }
            }
            

            this.labelP1Score.Text = engine.GetScore(p1).ToString();
            this.labelP2Score.Text = engine.GetScore(p2).ToString();
            this.labelTimeLeft.Text = engine.TimeLeft.ToString();

           // pictureBoxMap.Refresh();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonKill.Checked)
            {
                if (engine != null)
                {
                    draw_sellect_rectangle(mapRects[0,0], Color.Black);
                    ((HumanPlayer)engine.CurrentPlayer).QueueKill(0, 0);
                    this.isKilling = true;
                    isBrithing = false; 
                }
                button2.Text = "Kill";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (this.engine != null) { 
                this.engine.UpdateTime();
                DrawBoard(this.engine);
            }

            isKilling = false;
            isBrithing = false;
            radioButtonPass.Checked = true; 
            
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.engine != null)
            {
                this.engine.UpdateTime();
                DrawBoard(this.engine);
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonBirth.Checked)
            {
                isKilling = false;
                isBrithing = true; 
                button2.Text = "Birth";
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonPass.Checked)
            {
                if(engine != null)
                {

                    
                    ((HumanPlayer)engine.CurrentPlayer).QueuePass();
                }
                
                button2.Text = "Pass";
            }
        }

    
        private void draw_sellect_rectangle(Rectangle rec,Color col)
        {
            System.Drawing.SolidBrush myBrush;
            System.Drawing.Graphics formGraphics;
            formGraphics = pictureBoxMap.CreateGraphics();


            System.Drawing.Pen myPen = new System.Drawing.Pen(col, border_inner); 

            formGraphics.DrawRectangle(myPen, rec);
            //formGraphics.FillRectangle(myBrush, rec);
                
            

        }


        private void pictureBoxMap_SizeChanged(object sender, EventArgs e)
        {
            if(engine != null)
            {
                initalize_rectangles(engine);
                DrawBoard(engine);
            }
            

        }

        private void pictureBoxMap_Click(object sender, EventArgs e)
        {
            if (isKilling)
            {

                for (int x = 0; x < engine.boardWidth; x++)
                {
                    for (int y = 0; y < engine.boardHeight; y++)
                    {
                        if (mapRects[x, y].Contains(pictureBoxMap.PointToClient(MousePosition)))
                        {
                            DrawBoard(engine);
                            draw_sellect_rectangle(mapRects[x, y], Color.Black);
                            ((HumanPlayer)engine.CurrentPlayer).QueueKill(x, y);
                            
                        }
                    }
                }
            }
            //MessageBox.Show(pictureBoxMap.PointToClient(MousePosition).ToString());
        }
    }
}

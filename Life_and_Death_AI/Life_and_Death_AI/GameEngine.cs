﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms; 

namespace Life_and_Death_AI
{
    class GameEngine
    {
        public int boardWidth { get; }
        public int boardHeight { get; }
        Player player1;
        Player player2;
        Player currentPlayer;
        Player winner;
        int time = 0;
        int maxTime ; 

        GameCell[,] board; 
        public GameCell[,] Board { get { return board; } }
        public Player CurrentPlayer { get { return currentPlayer; } }
        public int TimeLeft { get { return maxTime-time; } }


        public GameEngine(Player player1, Player player2, int boardWidth, int boardHeight,int StartCellCount, int maxTime )
        {
            this.player1 = player1;
            this.player2 = player2;
            this.maxTime = maxTime; 

            if(new Random().Next(0, 1) == 0)
            {
                this.currentPlayer = this.player1;
            }
            else
            {
                this.currentPlayer = this.player2; 
            }

            this.boardHeight = boardHeight;
            this.boardWidth = boardWidth;
            board = new GameCell[boardWidth, boardHeight];
            this.IntiBoard(StartCellCount);

        }

        private void IntiBoard(int StartCellCount)
        {
            for (int x =0; x < boardWidth; x++)
            {
                for (int y =0; y < boardHeight; y++)
                {
                    board[x, y] = new GameCell(); 
                }
            }
            Random rnd = new Random(); 
            for (int i =0; i < StartCellCount; i++)
            {
                bool isAssigned = false; 
                while (!isAssigned)
                {
                    int x = rnd.Next(0, this.boardWidth);
                    int y = rnd.Next(0, this.boardHeight / 2); 
                    if(board[x,y].IsDead)
                    {
                        board[x, y].Spawn(player1); 
                        isAssigned = true;

                        board[boardWidth - x -1,boardHeight - y - 1].Spawn(player2);

                    }
                }
            }
        }

        public void UpdateTime()
        {
            if (time == maxTime)
            {
                return; 
            }
            this.time++; 
            
            // ask plyer for input

            GameAction acction = currentPlayer.DoPlay(this);
            this.handleAction(acction); 

            // swithc players 
            if ( currentPlayer == player1)
            {
                currentPlayer = player2;
            }

            if ( currentPlayer == player2)
            {
                currentPlayer = player1; 
            }

            // update map 

            
            GameCell [,] new_board = new GameCell[boardWidth, boardHeight];
            for (int x = 0; x < boardWidth; x++)
            {
                for (int y =0; y < boardHeight; y++)
                {
                    new_board[x, y] = new GameCell();
                }
            }

            for (int x = 0; x < boardWidth; x++)
            {
                for ( int y =0; y < boardHeight; y++)
                {
                    int nabor_cnt = 0;
                    int p1_cnt = 0;
                    int p2_cnt = 0; 
                    for (int j = -1; j <= 1; j++)
                    {
                        for (int k =-1; k<=1; k++)
                        {
                            if( x+j < 0             ||
                                x + j >= boardWidth ||
                                j == 0 && k == 0    ||
                                k+y < 0             ||
                                k+y >= boardHeight 
                                )
                            {
                                continue;
                            }
                            if (!board[x+j,y+k].IsDead)
                            {
                                nabor_cnt++; 
                                if(board[x + j, y + k].Owner == player1)
                                {
                                    p1_cnt++; 
                                }
                                if (board[x + j, y + k].Owner == player2)
                                {
                                    p2_cnt++;
                                }
                            }
                        }
                    }

                    if (!(nabor_cnt == 2 || nabor_cnt == 3) )
                    {
                        new_board[x, y].Kill();
                    }
                    if(!board[x,y].IsDead && (nabor_cnt == 2 || nabor_cnt == 3))
                    {
                        new_board[x, y] = board[x, y]; 
                    }
                    if (board[x, y].IsDead && nabor_cnt == 3)
                    {
                        if(p1_cnt > p2_cnt)
                        {
                            new_board[x, y].Spawn(player1); 
                        }
                        else
                        {
                            new_board[x, y].Spawn(player2);
                        }
                    }



                }
            }

            board = new_board;

            // check for vinner

            if (GetScore(player1) == 0 && GetScore(player2) == 0 || this.time == this.maxTime) 
            {
                MessageBox.Show("Draw");              
            }
            else if ( GetScore(player1) == 0)
            {
                MessageBox.Show("Player 1 winn");            
                this.winner = player1; 
            }
            else if (GetScore(player2) == 0)
            {
                MessageBox.Show("Player 2 winn");
                this.winner = player2; 
            }
            
            
        }

        void handleAction(GameAction action)
        {
            if ( action is GameActionKill)
            {
                GameActionKill a = (GameActionKill)action;
                board[a.TargetX, a.TargetY].Kill();
                MessageBox.Show("killing " + a.TargetX +"  "+ a.TargetY); 
                return; 
            }

            if (action is GameActionBirth)
            {
                GameActionBirth a = (GameActionBirth)action;
                throw new NotImplementedException(); 
                return;
            }

            if ( action is GameActionPass)
            {
                return; 
            }
        }

        public int GetScore(Player player)
        {
            int score = 0; 
            foreach (GameCell c in this.board)
            {
                if ( c.Owner == player)
                {
                    score++; 
                }
            }
            return score; 
        }
    }
}

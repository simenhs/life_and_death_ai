﻿namespace Life_and_Death_AI
{
    partial class Form_human_vs_human
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonStartGame = new System.Windows.Forms.Button();
            this.radioButtonKill = new System.Windows.Forms.RadioButton();
            this.radioButtonBirth = new System.Windows.Forms.RadioButton();
            this.radioButtonPass = new System.Windows.Forms.RadioButton();
            this.button2 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.labelP1text = new System.Windows.Forms.Label();
            this.labelP1Score = new System.Windows.Forms.Label();
            this.labelP2Score = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelTimeLeft = new System.Windows.Forms.Label();
            this.pictureBoxMap = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMap)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonStartGame
            // 
            this.buttonStartGame.Location = new System.Drawing.Point(256, 240);
            this.buttonStartGame.Name = "buttonStartGame";
            this.buttonStartGame.Size = new System.Drawing.Size(200, 100);
            this.buttonStartGame.TabIndex = 0;
            this.buttonStartGame.Text = "Start Game";
            this.buttonStartGame.UseVisualStyleBackColor = true;
            this.buttonStartGame.Click += new System.EventHandler(this.buttonStartGame_Click);
            // 
            // radioButtonKill
            // 
            this.radioButtonKill.AutoSize = true;
            this.radioButtonKill.Location = new System.Drawing.Point(12, 12);
            this.radioButtonKill.Name = "radioButtonKill";
            this.radioButtonKill.Size = new System.Drawing.Size(38, 17);
            this.radioButtonKill.TabIndex = 1;
            this.radioButtonKill.TabStop = true;
            this.radioButtonKill.Text = "Kill";
            this.radioButtonKill.UseVisualStyleBackColor = true;
            this.radioButtonKill.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButtonBirth
            // 
            this.radioButtonBirth.AutoSize = true;
            this.radioButtonBirth.Location = new System.Drawing.Point(12, 36);
            this.radioButtonBirth.Name = "radioButtonBirth";
            this.radioButtonBirth.Size = new System.Drawing.Size(46, 17);
            this.radioButtonBirth.TabIndex = 2;
            this.radioButtonBirth.TabStop = true;
            this.radioButtonBirth.Text = "Birth";
            this.radioButtonBirth.UseVisualStyleBackColor = true;
            this.radioButtonBirth.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButtonPass
            // 
            this.radioButtonPass.AutoSize = true;
            this.radioButtonPass.Checked = true;
            this.radioButtonPass.Location = new System.Drawing.Point(12, 59);
            this.radioButtonPass.Name = "radioButtonPass";
            this.radioButtonPass.Size = new System.Drawing.Size(48, 17);
            this.radioButtonPass.TabIndex = 3;
            this.radioButtonPass.TabStop = true;
            this.radioButtonPass.Text = "Pass";
            this.radioButtonPass.UseVisualStyleBackColor = true;
            this.radioButtonPass.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(13, 83);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(47, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Pass";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 172);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Player 1";
            
            // 
            // labelP1text
            // 
            this.labelP1text.AutoSize = true;
            this.labelP1text.Location = new System.Drawing.Point(9, 209);
            this.labelP1text.Name = "labelP1text";
            this.labelP1text.Size = new System.Drawing.Size(45, 13);
            this.labelP1text.TabIndex = 6;
            this.labelP1text.Text = "Player 2";
            // 
            // labelP1Score
            // 
            this.labelP1Score.AutoSize = true;
            this.labelP1Score.Location = new System.Drawing.Point(12, 189);
            this.labelP1Score.Name = "labelP1Score";
            this.labelP1Score.Size = new System.Drawing.Size(13, 13);
            this.labelP1Score.TabIndex = 7;
            this.labelP1Score.Text = "0";
            // 
            // labelP2Score
            // 
            this.labelP2Score.AutoSize = true;
            this.labelP2Score.Location = new System.Drawing.Point(9, 222);
            this.labelP2Score.Name = "labelP2Score";
            this.labelP2Score.Size = new System.Drawing.Size(13, 13);
            this.labelP2Score.TabIndex = 8;
            this.labelP2Score.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 267);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Time left";
            // 
            // labelTimeLeft
            // 
            this.labelTimeLeft.AutoSize = true;
            this.labelTimeLeft.Location = new System.Drawing.Point(15, 284);
            this.labelTimeLeft.Name = "labelTimeLeft";
            this.labelTimeLeft.Size = new System.Drawing.Size(13, 13);
            this.labelTimeLeft.TabIndex = 10;
            this.labelTimeLeft.Text = "0";
            // 
            // pictureBoxMap
            // 
            this.pictureBoxMap.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxMap.BackColor = System.Drawing.SystemColors.HotTrack;
            this.pictureBoxMap.Location = new System.Drawing.Point(102, 12);
            this.pictureBoxMap.Name = "pictureBoxMap";
            this.pictureBoxMap.Size = new System.Drawing.Size(666, 519);
            this.pictureBoxMap.TabIndex = 11;
            this.pictureBoxMap.TabStop = false;
            this.pictureBoxMap.Visible = false;
            this.pictureBoxMap.SizeChanged += new System.EventHandler(this.pictureBoxMap_SizeChanged);
            this.pictureBoxMap.Click += new System.EventHandler(this.pictureBoxMap_Click);
            // 
            // Form_human_vs_human
            // 
            this.ClientSize = new System.Drawing.Size(780, 543);
            this.Controls.Add(this.pictureBoxMap);
            this.Controls.Add(this.labelTimeLeft);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelP2Score);
            this.Controls.Add(this.labelP1Score);
            this.Controls.Add(this.labelP1text);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.radioButtonPass);
            this.Controls.Add(this.radioButtonBirth);
            this.Controls.Add(this.radioButtonKill);
            this.Controls.Add(this.buttonStartGame);
            this.Name = "Form_human_vs_human";
            
            this.SizeChanged += new System.EventHandler(this.Form_human_vs_human_SizeChanged);
           
            
          
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMap)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

       
        private System.Windows.Forms.Button buttonStartGame;
        private System.Windows.Forms.RadioButton radioButtonKill;
        private System.Windows.Forms.RadioButton radioButtonBirth;
        private System.Windows.Forms.RadioButton radioButtonPass;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelP1text;
        private System.Windows.Forms.Label labelP1Score;
        private System.Windows.Forms.Label labelP2Score;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelTimeLeft;
        private System.Windows.Forms.PictureBox pictureBoxMap;
    }
}
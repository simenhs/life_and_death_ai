﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Life_and_Death_AI
{
    

    class GameCell
    {
        Player owner; 

        public GameCell()
        {

        }
        public Player Owner { get { return this.owner; } } 

        public bool IsDead
        {
            get
            {
                if((object)this.owner == null)
                {
                    return true;
                }
                return false; 
            }
        }
        
        public void Kill()
        {
            this.owner = null; 
        }

        public void Spawn(Player player)
        {
            this.owner = player; 
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Life_and_Death_AI
{
    abstract class GameAction
    {
    }

    class GameActionPass :GameAction
    {

    }
    class GameActionKill : GameAction
    {
        int x;
        int y; 
        public int TargetX { get { return x; } }
        public int TargetY { get { return y; } }
        public GameActionKill(int targetX, int targetY)
        {
            this.x = targetX;
            this.y = targetY; 
        }
    }
    class GameActionBirth : GameAction
    {

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Life_and_Death_AI 
{
    class HumanPlayer : Player
    {
        GameAction queuedAction = new GameActionPass(); 

        public override GameAction DoPlay(GameEngine engine)
        {
            return this.queuedAction;
        }
        public void QueuePass()
        {
            queuedAction = new GameActionPass();
        }
        public void QueueKill(int x, int y)
        {
            queuedAction = new GameActionKill(x,y);
        }
    }
}
